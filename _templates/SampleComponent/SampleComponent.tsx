import React from 'react'

import * as Styled from './SampleComponent.styled'

export interface SampleComponentProps {
  headline?: string
  subheadline?: string
  body?: string
}

export const SampleComponent: React.FunctionComponent<SampleComponentProps> = (
  props
) => {
  const { headline, subheadline, body } = props

  const [open, setOpen] = React.useState<boolean>(false)

  return (
    <Styled.Wrapper>
      <Styled.Container>
        {headline && (
          <Styled.Headline dangerouslySetInnerHTML={{ __html: headline }} />
        )}
        {subheadline && (
          <Styled.Subheadline
            dangerouslySetInnerHTML={{ __html: subheadline }}
          />
        )}
        {body && <Styled.Body dangerouslySetInnerHTML={{ __html: body }} />}
      </Styled.Container>
    </Styled.Wrapper>
  )
}
