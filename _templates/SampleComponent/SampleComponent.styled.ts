import styled from '@emotion/styled'

export const Wrapper = styled.section``

export const Container = styled.div``

export const Headline = styled.h2``

export const Subheadline = styled.p``

export const Body = styled.div``
