# Bulletproof Full-stack NextJS Boilerplate

A NextJS boilerplate project that will make sure you have consistent code across other developers. This project uses ESLint to enforce rules, Prettier to automatically format the code, TypeScript to minimize unexpected runtime errors, and Husky to run automated tasks during git actions.

## Features

- 🔥 **TypeScript** for Type checking
- ❗ **Strict Mode** to TypeScript and React 18
- 📏 Linter using **ESLint**
- 💖 Code Formatter using **Prettier**
- 🦊 **Husky** for Git Hooks
- 👩‍🎤 CSS-in-JS using **Emotion**
- ⚛️ **Redux Toolkit** for easy State Management

## Requirements

- ✅ Visual Studio Code
- ✅ NodeJS (^16.0.0)
- ✅ Yarn (^1.22.0)

## Common setup

Clone the repository.

```bash
git clone git@bitbucket.org:isabelonicolas/bulletproof-full-stack-nextjs-boilerplate.git
```

Go to the project directory.

```bash
cd bulletproof-full-stack-nextjs-boilerplate
```

Install the dependencies.

```bash
yarn
```

Run the application in development mode.

```bash
yarn dev
```

Build the application for production usage

```bash
yarn build
```

Run the production server

```bash
yarn start
```
