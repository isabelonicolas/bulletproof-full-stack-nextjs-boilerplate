export const seo = {
  title: `NextJS`,
  description: `Production grade React applications that scale. The world’s leading companies use Next.js by Vercel to build static and dynamic websites and web applications.`,
  image: `${process.env.NEXT_PUBLIC_HOST}/images/default-seo-image.jpg`,
}

export const gtmTrackingId = process.env.NEXT_PUBLIC_GA_TRACKING_ID || ''
