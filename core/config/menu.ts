export const primaryMenu = [
  {
    label: 'Home',
    title: 'Home',
    url: '/',
  },
  {
    label: 'About',
    title: 'About',
    url: '/about',
  },
  {
    label: 'RTK',
    title: 'RTK',
    url: '/rtk',
  },
]
