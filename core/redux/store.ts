import { configureStore } from '@reduxjs/toolkit'

import counterReducer from '@/redux/features/counter/counterSlice'
import { postsReducer } from '@/redux/features/posts/postsSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    posts: postsReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

export default store
