import { createAsyncThunk } from '@reduxjs/toolkit'

import { delay } from '@/utils/delay'

export const getPosts = createAsyncThunk('posts/getPosts', async (thunkAPI) => {
  await delay(1000)

  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  const posts = await res.json()

  return posts
})
