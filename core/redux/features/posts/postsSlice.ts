import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

import { getPosts } from '@/redux/features/posts/postsThunks'

type PostType = {
  id: number
  title: string
  body: string
}

interface PostsStateType {
  entities: PostType[]
  loading: boolean
}

const initialState: PostsStateType = {
  entities: [],
  loading: false,
}

export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder.addCase(getPosts.pending, (state) => {
      state.loading = true
    })

    builder.addCase(
      getPosts.fulfilled,
      (state, action: PayloadAction<PostType[]>) => {
        const { payload } = action

        state.loading = false
        state.entities = payload
      }
    )

    builder.addCase(getPosts.rejected, (state) => {
      state.loading = false
    })
  },
})

// export const {} = postsSlice.actions

export const postsReducer = postsSlice.reducer
