import { gtmTrackingId } from '@/config/site'

export interface GTagPageViewOptions {
  page_title?: string
  page_location?: string
  page_path?: string
  send_page_view?: boolean
}

export type GTagEventBaseOptions = {
  event_category?: string
  event_label?: string
  value?: number
}

export interface GTagEventOptions extends GTagEventBaseOptions {
  [otherOptions: string]: unknown
}

/**
 * Sends page view to Google
 *
 * @param {Object} options - The page data to be passed to Google.
 * @param {string|undefined} options.page_title - The title of the page.
 * @param {string|undefined} options.page_location - The URL of the page.
 * @param {string|undefined} options.page_path - The path to the page. If overridden, this value must start with a / character.
 * @param {boolean|undefined} options.send_page_view - Whether or not a pageview should be sent.
 *
 * @see {@link https://developers.google.com/analytics/devguides/collection/gtagjs/pages}
 */
export const pageView = (options?: GTagPageViewOptions) => {
  window.gtag('config', gtmTrackingId, {
    ...options,
  })
}

/**
 * Sends page events to Google
 *
 * @param {string} action - The value that will appear as the event action in Google Analytics Event reports.
 * @param {Object|undefined} options - Optional parameters.
 * @param {string|undefined} options.event_category - The category of the event.
 * @param {string|undefined} options.event_label - The label of the event.
 * @param {number|undefined} options.value - A non-negative integer that will appear as the event value.
 * @param {...*} options.var_args - Any custom values
 *
 * @see {@link https://developers.google.com/analytics/devguides/collection/gtagjs/events}
 */

export const event = (action: string, options: GTagEventOptions) => {
  window.gtag('event', action, {
    ...options,
  })
}
