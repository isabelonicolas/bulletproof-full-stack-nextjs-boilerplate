import { useEffect, useLayoutEffect } from 'react'

/**
 * useIsomorphicEffect hook
 * Resolves to useEffect when "window" is not in scope and useLayout effect in the browser
 */
export const useIsomorphicEffect =
  typeof window === 'undefined' ? useEffect : useLayoutEffect
