import React from 'react'

import { useIsomorphicEffect } from '@/hooks/useIsomorphicEffect'

type ReturnType = {
  locked: boolean

  setLocked: (locked: boolean) => void
}

/**
 * useLockedBody hook
 * A hook that for blocking the scrolling of the page.
 *
 * @returns {object} Returns the status and setter function of the hook
 */
export const useLockedBody = (): ReturnType => {
  const [locked, setLocked] = React.useState(false)

  // Do the side effect before render.
  useIsomorphicEffect(() => {
    if (!locked) {
      return
    }

    const scrollPosition = window.pageYOffset

    // Save initial body style.
    const originalCSSProperties = {
      overflow: document.body.style.overflow,
      width: document.body.style.width,
      position: document.body.style.position,
      top: document.body.style.top,
    }

    // Lock body scroll.
    document.body.style.overflow = 'hidden'
    document.body.style.width = '100%'
    document.body.style.position = 'fixed'
    document.body.style.top = `-${scrollPosition}px`

    return () => {
      // Revert back to original settings on unmount.
      document.body.style.overflow = originalCSSProperties.overflow
      document.body.style.width = originalCSSProperties.width
      document.body.style.position = originalCSSProperties.position
      document.body.style.top = originalCSSProperties.top

      window.scrollTo(0, scrollPosition)
    }
  }, [locked])

  return {
    locked: locked,
    setLocked: setLocked,
  }
}
