import React from 'react'
import { useRouter } from 'next/router'

type ReturnType = void

/**
 * useRouteChange hook
 * A hook that listens to next router's routeChangeComplete, and hashChangeComplete events
 *
 * @param {function} onRouteChange - The function to be called after route change event.
 * @returns {undefined}
 */
export const useRouteChange = (
  onRouteChange: (path: string) => void
): ReturnType => {
  const router = useRouter()

  React.useEffect(() => {
    const handler = (path: string) => {
      onRouteChange(path)
    }

    router.events.on('routeChangeComplete', handler)
    router.events.on('hashChangeComplete', handler)

    return () => {
      router.events.off('routeChangeComplete', handler)
      router.events.off('hashChangeComplete', handler)
    }
  }, [router.events, onRouteChange])
}
