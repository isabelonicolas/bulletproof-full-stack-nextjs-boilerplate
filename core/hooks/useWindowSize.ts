import { useState } from 'react'

import { useIsomorphicEffect } from '@/hooks/useIsomorphicEffect'

type WindowDimensions = {
  innerWidth: number | null
  innerHeight: number | null
  outerWidth: number | null
  outerHeight: number | null
}

const getDimensions = (): WindowDimensions => {
  return {
    innerHeight: window.innerHeight,
    innerWidth: window.innerWidth,
    outerHeight: window.outerHeight,
    outerWidth: window.outerWidth,
  }
}

/**
 * useWindowSize hook
 * A hook that provides information of the dimensions of the window
 *
 * @returns {object} Returns the dimensions of the window
 */
export const useWindowSize = (): WindowDimensions => {
  const [windowSize, setWindowSize] = useState<WindowDimensions>({
    innerHeight: null,
    innerWidth: null,
    outerHeight: null,
    outerWidth: null,
  })

  // Set resize handler once on mount and clean before unmount.
  useIsomorphicEffect(() => {
    function resizeHandler() {
      setWindowSize(getDimensions())
    }

    resizeHandler()

    if (typeof window !== 'undefined') {
      window.addEventListener('resize', resizeHandler)

      return () => {
        window.removeEventListener('resize', resizeHandler)
      }
    } else {
      console.warn('useWindowSize: window is undefined.')
    }
  }, [])

  return windowSize
}
