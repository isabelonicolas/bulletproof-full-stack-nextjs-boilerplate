import { Theme, css } from '@emotion/react'
import { rem } from 'polished'

import { effra, roboto } from '@/styles/fonts'

const layout = {
  app: {
    height: `var(--app-height)`,
  },

  container: {
    width: rem('1400px'),
    gutter: rem('20px'),
  },

  column: {
    gutter: rem('15px'),
  },

  sidebar: {
    width: rem('320px'),
  },
}

const breakpoint = {
  min: `320px`,
  xs: `375px`,
  sm: `576px`,
  md: `768px`,
  lg: `992px`,
  xl: `1200px`,
  xxl: `1400px`,
  xxxl: `1600px`,
  fhd: `1920px`,
}

const media = {
  min: `@media (min-width: 320px)`,
  xs: `@media (min-width: 375px)`,
  sm: `@media (min-width: 576px)`,
  md: `@media (min-width: 768px)`,
  lg: `@media (min-width: 992px)`,
  xl: `@media (min-width: 1200px)`,
  xxl: `@media (min-width: 1400px)`,
  xxxl: `@media (min-width: 1600px)`,
  fhd: `@media (min-width: 1920px)`,
}

const mixins = {
  container: css`
    width: 100%;
    max-width: ${layout.container.width};
    margin-right: auto;
    margin-left: auto;
    padding-right: ${layout.container.gutter};
    padding-left: ${layout.container.gutter};
  `,
}

const fonts = {
  body: effra.style.fontFamily,
  body2: roboto.style.fontFamily,
}

export const theme: Theme = {
  layout,
  breakpoint,
  media,
  mixins,
  fonts,
}
