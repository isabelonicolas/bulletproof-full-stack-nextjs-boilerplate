import { Roboto } from 'next/font/google'
import localFont from 'next/font/local'

export const roboto = Roboto({
  weight: ['400', '700'],
  style: ['normal', 'italic'],
  subsets: ['latin'],
})

export const effra = localFont({
  src: [
    {
      path: '../assets/fonts/typeface-effra/files/effra-300.woff2',
      weight: '300',
      style: 'normal',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-300italic.woff2',
      weight: '300',
      style: 'italic',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-400.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-400italic.woff2',
      weight: '400',
      style: 'italic',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-500.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-500italic.woff2',
      weight: '500',
      style: 'italic',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-700.woff2',
      weight: '700',
      style: 'normal',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-700italic.woff2',
      weight: '700',
      style: 'italic',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-900.woff2',
      weight: '900',
      style: 'normal',
    },
    {
      path: '../assets/fonts/typeface-effra/files/effra-900italic.woff2',
      weight: '900',
      style: 'italic',
    },
  ],
})
