import React from "react";

export interface AccordionContextProps {
	isExpanded: (index?: number) => boolean;
	toggle: (index?: number) => void;
}

export interface AccordionProviderProps {
	children: (accordion: AccordionContextProps) => React.ReactElement;
}

const AccordionContext = React.createContext<AccordionContextProps>({
	isExpanded: () => false,
	toggle: () => {},
});

export const AccordionProvider: React.FunctionComponent<
	AccordionProviderProps
> = (props) => {
	const { children } = props;

	const [activeIndices, setActiveIndices] = React.useState<Array<number>>([]);

	const toggle = (selectedIndex?: number) => {
		if (typeof selectedIndex === "number") {
			if (activeIndices.indexOf(selectedIndex) === -1) {
				setActiveIndices((prevState) => {
					let newState = [...prevState, selectedIndex];

					return newState;
				});
			} else {
				setActiveIndices((prevState) => {
					let newState = [...prevState];

					newState.splice(newState.indexOf(selectedIndex), 1);

					return newState;
				});
			}
		}
	};

	const isExpanded = (selectedIndex?: number) => {
		if (typeof selectedIndex !== "number") return false;

		return activeIndices.indexOf(selectedIndex) !== -1;
	};

	const value = {
		isExpanded,
		toggle,
	};

	return (
		<AccordionContext.Provider value={value}>
			{children(value)}
		</AccordionContext.Provider>
	);
};

export const useAccordion = () => {
	return React.useContext(AccordionContext);
};
