import React from 'react'
import { cx } from '@emotion/css'

import {
  AccordionContextProps,
  AccordionProvider,
} from '@/components/Accordion/AccordionProvider'

import scss from '@/components/Accordion/index.module.scss'

export interface AccordionProps {
  children: (context: AccordionContextProps) => React.ReactElement
  className?: string
}

export const Accordion: React.FunctionComponent<AccordionProps> = (props) => {
  const { children, className } = props

  return (
    <AccordionProvider>
      {(context) => (
        <div className={cx(scss.accordion, className)}>{children(context)}</div>
      )}
    </AccordionProvider>
  )
}
