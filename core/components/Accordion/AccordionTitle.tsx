import React from 'react'
import { cx } from '@emotion/css'

import { useAccordion } from '@/components/Accordion/AccordionProvider'

import scss from '@/components/Accordion/index.module.scss'

export interface AccordionTitleProps {
  children: React.ReactNode
  className?: string
  index?: number
}

export const AccordionTitle: React.FunctionComponent<AccordionTitleProps> = (
  props
) => {
  const { children, className, index } = props

  const { toggle } = useAccordion()

  const onClickHandler = (evt: React.MouseEvent<HTMLDivElement>) => {
    toggle(index)
  }

  const onKeyUpHandler = (evt: React.KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      toggle(index)
    }
  }

  return (
    <div
      className={cx(scss.accordion_title, className)}
      tabIndex={0}
      onClick={onClickHandler}
      onKeyUp={onKeyUpHandler}
    >
      {children}
    </div>
  )
}
