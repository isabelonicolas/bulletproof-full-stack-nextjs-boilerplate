import React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionTitle,
  AccordionContent,
} from '@/components/Accordion'

import scss from '@/components/Accordion/examples/example1.module.scss'

export const Example1: React.FunctionComponent = () => {
  return (
    <Accordion className={scss.accordion}>
      {({ isExpanded, toggle }) => (
        <>
          <AccordionItem className={scss.accordion_item} index={1}>
            <AccordionTitle className={scss.accordion_title}>
              <span>Lorem ipsum dolor sit amet</span>
              <span>{isExpanded(1) ? 'Collapse' : 'Expand'}</span>
            </AccordionTitle>
            <AccordionContent className={scss.accordion_content}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
              <br />
              <br />
              <button type="button" title="Click me" onClick={() => toggle(1)}>
                Click me!
              </button>
            </AccordionContent>
          </AccordionItem>
          <AccordionItem className={scss.accordion_item} index={2}>
            <AccordionTitle className={scss.accordion_title}>
              <span>Lorem ipsum dolor sit amet</span>
              <span>{isExpanded(2) ? 'Collapse' : 'Expand'}</span>
            </AccordionTitle>
            <AccordionContent className={scss.accordion_content}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
              <br />
              <br />
              <button type="button" title="Click me" onClick={() => toggle(2)}>
                Click me!
              </button>
            </AccordionContent>
          </AccordionItem>
          <AccordionItem className={scss.accordion_item} index={3}>
            <AccordionTitle className={scss.accordion_title}>
              <span>Lorem ipsum dolor sit amet</span>
              <span>{isExpanded(3) ? 'Collapse' : 'Expand'}</span>
            </AccordionTitle>
            <AccordionContent className={scss.accordion_content}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
              <br />
              <br />
              <button type="button" title="Click me" onClick={() => toggle(3)}>
                Click me!
              </button>
            </AccordionContent>
          </AccordionItem>
        </>
      )}
    </Accordion>
  )
}
