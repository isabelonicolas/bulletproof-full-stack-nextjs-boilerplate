import React from 'react'
import { cx } from '@emotion/css'
import { motion } from 'framer-motion'

import { useAccordion } from '@/components/Accordion/AccordionProvider'

import scss from '@/components/Accordion/index.module.scss'

export interface AccordionContentProps {
  children: React.ReactNode
  className?: string
  index?: number
}

export const AccordionContent: React.FunctionComponent<
  AccordionContentProps
> = (props) => {
  const { children, className, index } = props

  const { isExpanded } = useAccordion()

  return (
    <motion.div
      className={cx(scss.accordion_content)}
      variants={{
        expand: {
          height: 'auto',
          opacity: 1,
          y: 0,
          display: 'block',
        },
        collapse: {
          height: 0,
          opacity: 0,
          y: -15,
          transitionEnd: {
            display: 'none',
          },
        },
      }}
      initial="collapse"
      animate={isExpanded(index) ? 'expand' : 'collapse'}
      transition={{ duration: 0.25 }}
    >
      <div className={cx(scss.accordion_content_inner, className)}>
        {children}
      </div>
    </motion.div>
  )
}
