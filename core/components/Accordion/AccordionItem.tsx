import React from 'react'
import { cx } from '@emotion/css'

import scss from '@/components/Accordion/index.module.scss'

export interface AccordionItemProps {
  children: React.ReactNode
  className?: string
  index: number
}

export const AccordionItem: React.FunctionComponent<AccordionItemProps> = (
  props
) => {
  const { children, className, index } = props

  const childrenWithProps = React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(
        child as React.ReactElement<{ index?: number }>,
        {
          index: index,
        }
      )
    }

    return child
  })

  return (
    <div className={cx(scss.accordion_item, className)}>
      {childrenWithProps}
    </div>
  )
}
