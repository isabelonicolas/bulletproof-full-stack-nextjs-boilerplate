import Script from 'next/script'

import { useRouteChange } from '@/hooks/useRouteChange'
import * as gtag from '@/lib/gtag'

export interface GTMSetupProps {
  trackingId: string
}

export const GTMSetup: React.FC<GTMSetupProps> = (props) => {
  const { trackingId } = props

  useRouteChange((path) => {
    gtag.pageView({ page_path: path })
    gtag.event('site_route_change', {
      event_category: 'event_category',
      event_label: 'event_label',
      value: 1,
      custom_value: 'This is a custom value',
    })
  })

  return (
    <>
      <Script
        strategy="afterInteractive"
        src={`https://www.googletagmanager.com/gtag/js?id=${trackingId}`}
      />
      <Script
        id="gtm-init"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
          
            gtag('config', '${trackingId}', {
              page_path: window.location.pathname,
            });
          `,
        }}
      />
    </>
  )
}
