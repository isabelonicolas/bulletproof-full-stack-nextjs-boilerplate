import { SEOProps } from '@/components/SEO/SEO'
import { App, AppContent, AppMain, AppWrapper } from '@/components/Layout/parts'

interface BlankLayoutProps {
  seo?: SEOProps
  children?: React.ReactNode
}

export const BlankLayout: React.FC<BlankLayoutProps> = (props) => {
  const { seo, children } = props

  return (
    <AppWrapper seo={seo}>
      <App>
        <AppMain fullWidth>
          <AppContent>{children}</AppContent>
        </AppMain>
      </App>
    </AppWrapper>
  )
}
