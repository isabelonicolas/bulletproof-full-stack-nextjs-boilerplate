import styled from '@emotion/styled'

import { useWindowSize } from '@/hooks/useWindowSize'

interface AppProps {
  children?: React.ReactNode
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  min-height: ${({ theme }) => theme.layout.app.height};
  position: relative;
`

export const App: React.FC<AppProps> = (props) => {
  const { children } = props

  const { innerHeight } = useWindowSize()

  if (typeof window !== 'undefined') {
    document.documentElement.style.setProperty(
      '--app-height',
      `${innerHeight}px`
    )
  }

  return <Wrapper>{children}</Wrapper>
}
