import styled from '@emotion/styled'

interface AppFooterProps {
  children?: React.ReactNode
}

const Wrapper = styled.footer`
  position: relative;
`

export const AppFooter: React.FC<AppFooterProps> = (props) => {
  const { children } = props

  return <Wrapper>{children}</Wrapper>
}
