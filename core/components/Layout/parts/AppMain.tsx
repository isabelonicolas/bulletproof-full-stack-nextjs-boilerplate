import styled from '@emotion/styled'
import { rem } from 'polished'

interface AppMainProps {
  children?: React.ReactNode
  fullWidth?: boolean
}

const Wrapper = styled.main<{ fullWidth: boolean }>`
  flex: 0 0 auto;
  position: relative;
  display: flex;
  flex-flow: row wrap;
  width: 100%;
  max-width: ${({ theme }) => theme.layout.container.width};
  margin-right: auto;
  margin-left: auto;
  margin-bottom: auto;

  ${({ theme }) => theme.media.lg} {
    margin-bottom: 0;
    flex: 1;
  }

  ${({ fullWidth }) =>
    fullWidth &&
    `
      max-width: 100%;
      padding-right: 0;
      padding-left: 0;
  `}
`

export const AppMain: React.FC<AppMainProps> = (props) => {
  const { children, fullWidth = false } = props

  return <Wrapper fullWidth={fullWidth}>{children}</Wrapper>
}
