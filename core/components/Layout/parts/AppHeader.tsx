import styled from '@emotion/styled'

interface AppHeaderProps {
  children?: React.ReactNode
}

const Wrapper = styled.header`
  position: relative;
`

export const AppHeader: React.FC<AppHeaderProps> = (props) => {
  const { children } = props

  return <Wrapper>{children}</Wrapper>
}
