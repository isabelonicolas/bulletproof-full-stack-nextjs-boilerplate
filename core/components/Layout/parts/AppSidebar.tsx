import styled from '@emotion/styled'
import { rem } from 'polished'

interface AppSidebarProps {
  children?: React.ReactNode
}

const Wrapper = styled.aside`
  position: relative;
  order: 2;
  flex: 0 0 100%;
  max-width: 100%;
  margin-top: ${({ theme }) => rem(theme.layout.container.gutter)};

  ${({ theme }) => theme.media.lg} {
    order: unset;
    flex: 0 0 ${({ theme }) => rem(theme.layout.sidebar.width)};
    max-width: ${({ theme }) => rem(theme.layout.sidebar.width)};
    margin-top: 0;
  }
`

const Inner = styled.div`
  position: sticky;
  top: 0;
`

export const AppSidebar: React.FC<AppSidebarProps> = (props) => {
  const { children } = props

  return (
    <Wrapper>
      <Inner>{children}</Inner>
    </Wrapper>
  )
}
