import styled from '@emotion/styled'

interface AppContentProps {
  children?: React.ReactNode
}

const Wrapper = styled.div`
  position: relative;
  order: 1;
  flex: 0 0 100%;
  max-width: 100%;

  ${({ theme }) => theme.media.lg} {
    flex: 1;
    order: unset;
  }
`

export const AppContent: React.FC<AppContentProps> = (props) => {
  const { children } = props

  return <Wrapper>{children}</Wrapper>
}
