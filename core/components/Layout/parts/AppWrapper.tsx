import styled from '@emotion/styled'

import { SEO, SEOProps } from '@/components/SEO/SEO'

interface AppWrapperProps {
  seo?: SEOProps
  children?: React.ReactNode
}

const Wrapper = styled.div`
  position: relative;
`

export const AppWrapper: React.FC<AppWrapperProps> = (props) => {
  const { seo, children } = props

  return (
    <Wrapper>
      <SEO
        title={seo?.title}
        description={seo?.description}
        image={seo?.image}
        meta={seo?.meta}
      />
      {children}
    </Wrapper>
  )
}
