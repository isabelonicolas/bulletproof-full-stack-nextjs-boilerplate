import { SEOProps } from '@/components/SEO/SEO'
import { Navbar } from '@/components/NavBar/NavBar'
import { Footer } from '@/components/Footer/Footer'
import {
  App,
  AppContent,
  AppFooter,
  AppHeader,
  AppMain,
  AppWrapper,
} from '@/components/Layout/parts'

interface StandardLayoutProps {
  seo?: SEOProps
  children?: React.ReactNode
}

export const StandardLayout: React.FC<StandardLayoutProps> = (props) => {
  const { seo, children } = props

  return (
    <AppWrapper seo={seo}>
      <App>
        <AppHeader>
          <Navbar />
        </AppHeader>
        <AppMain fullWidth>
          <AppContent>{children}</AppContent>
        </AppMain>
        <AppFooter>
          <Footer />
        </AppFooter>
      </App>
    </AppWrapper>
  )
}
