import React from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'

import { seo } from '@/config/site'

type Meta = {
  name: {
    key: string
    value: string
  }
  content: {
    key: string
    value: string
  }
}

export interface SEOProps {
  title?: string
  description?: string
  image?: string
  meta?: Meta[]
}

export const SEO: React.FC<SEOProps> = (props) => {
  const { title, description, image, meta = [] } = props

  const { pathname } = useRouter()

  const seoTitle = title ? `${seo.title} | ${title}` : seo.title
  const seoDescription = description || seo.description
  const seoImage = image || seo.image
  const seoUrl = `${process.env.NEXT_PUBLIC_HOST}${pathname}`

  return (
    <Head>
      <title>{seoTitle}</title>
      <meta name="description" content={seoDescription} />

      {/* Open Graph */}
      <meta property="og:type" content="website" />
      <meta property="og:title" content={seoTitle} />
      <meta property="og:description" content={seoDescription} />
      <meta property="og:image" content={seoImage} />
      <meta property="og:url" content={seoUrl} />

      {/* Twitter */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:creator" content="imnii26" />
      <meta property="twitter:title" content={seoTitle} />
      <meta property="twitter:description" content={seoDescription} />
      <meta property="twitter:image" content={seoImage} />
      <meta property="twitter:url" content={seoUrl} />

      {/* Other Meta Tags */}
      {meta.length > 0 && (
        <>
          {meta.map((metaItem, metaItemIndex) => {
            const properties = {
              [metaItem.name.key]: metaItem.name.value,
              [metaItem.content.key]: metaItem.content.value,
            }

            return <meta key={`metaTag${metaItemIndex}`} {...properties} />
          })}
        </>
      )}
    </Head>
  )
}
