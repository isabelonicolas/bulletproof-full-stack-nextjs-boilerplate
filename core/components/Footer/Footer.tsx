import React from 'react'

import { Wrapper, Container, Copyright } from '@/components/Footer/Footer.style'

export interface FooterProps {}

export const Footer: React.FC<FooterProps> = (props) => {
  const year = new Date().getFullYear()

  return (
    <Wrapper>
      <Container>
        <Copyright>© {year} business.com. All Rights Reserved.</Copyright>
      </Container>
    </Wrapper>
  )
}
