import styled from '@emotion/styled'
import { rem } from 'polished'

export const Wrapper = styled.section`
  border-top: ${rem(1)} solid #f1f1f1;
  padding-top: ${rem(20)};
  padding-bottom: ${rem(20)};
  background-color: #ffffff;
`

export const Container = styled.div`
  ${({ theme }) => theme.mixins.container}
`

export const Copyright = styled.p`
  margin: 0;
  text-align: center;
`
