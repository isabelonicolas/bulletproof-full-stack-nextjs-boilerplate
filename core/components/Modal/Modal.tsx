import React from 'react'
import ReactDOM from 'react-dom'
import styled from '@emotion/styled'
import { hideVisually, rem } from 'polished'
import { AnimatePresence, motion } from 'framer-motion'

import { useLockedBody } from '@/hooks/useLockedBody'

export interface ModalProps {
  id: string
  title: string
  isOpen: boolean
  noPadding?: boolean
  children?: React.ReactNode

  onClose?: () => void
  onOpen?: () => void
}

const Wrapper = styled(motion.div)`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: ${({ theme }) => rem(theme.layout.container.gutter)};
  overflow-x: hidden;
  overflow-y: auto;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  outline: 0;
  background-color: rgb(0 0 0 / 75%);
`

const Dialog = styled(motion.div)`
  display: block;
  width: 100%;
  max-width: ${rem(500)};
  margin: 0 auto auto auto;
  position: relative;
  pointer-events: none;
`

const Content = styled.div<{ noPadding: boolean }>`
  display: block;
  width: 100%;
  padding: ${({ noPadding }) => (noPadding ? `0` : rem(15))};
  position: relative;
  z-index: 1;
  pointer-events: auto;
  outline: 0;
  background-color: #ffffff;
  border-radius: ${rem(4)};
  box-shadow: 0 0 ${rem(2)} ${rem(2)} rgb(0 0 0 / 25%);
`
const ButtonClose = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${rem(28)};
  height: ${rem(28)};
  margin: 0;
  padding: 0;
  position: absolute;
  top: 0;
  right: 0;
  border-radius: 50%;
  border: ${rem(2)} solid transparent;
  background-color: #ffffff;
  color: inherit;
  cursor: pointer;
  box-shadow: 0 0 ${rem(2)} ${rem(2)} rgb(0 0 0 / 25%);
  transform: translate(30%, -30%);
`

const Title = styled.h5`
  ${hideVisually()};
`

const motionFadeIn = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
  },
}

const motionZoomIn = {
  hidden: {
    y: -100,
  },
  visible: {
    y: 0,
  },
}

export const Modal: React.FC<ModalProps> = (props) => {
  const {
    id,
    title,
    isOpen,
    noPadding = false,
    children,
    onClose,
    onOpen,
  } = props

  const { setLocked } = useLockedBody()

  function closeModal() {
    if (!onClose) return

    onClose()
  }

  function onModalReadyHandler() {
    if (!onOpen) return

    onOpen()
  }

  function onOutsideClickHandler() {
    closeModal()
  }

  function onAnimationStartHandler(definition: string) {
    if (definition === 'visible') {
      setLocked(true)
    }
  }

  function onAnimationCompleteHandler(definition: string) {
    if (definition === 'visible') {
      onModalReadyHandler()
    }

    if (definition === 'hidden') {
      setLocked(false)
    }
  }

  if (typeof window !== 'undefined') {
    return ReactDOM.createPortal(
      <AnimatePresence mode="wait">
        {isOpen && (
          <Wrapper
            tabIndex={-1}
            role="dialog"
            aria-labelledby={`${id}Label`}
            aria-modal={isOpen}
            variants={motionFadeIn}
            transition={{
              ease: 'easeInOut',
              duration: 0.25,
            }}
            initial="hidden"
            animate="visible"
            exit="hidden"
            onClick={onOutsideClickHandler}
            onAnimationStart={onAnimationStartHandler}
            onAnimationComplete={onAnimationCompleteHandler}
          >
            <Dialog
              role="document"
              variants={motionZoomIn}
              transition={{
                ease: 'easeInOut',
                duration: 0.25,
              }}
              initial="hidden"
              animate="visible"
              exit="hidden"
              onClick={(evt) => evt.stopPropagation()}
            >
              <Content noPadding={noPadding}>
                <ButtonClose
                  type="button"
                  title="Close"
                  aria-label="Close"
                  onClick={closeModal}
                >
                  <i className="fa-solid fa-xmark" />
                </ButtonClose>
                <Title id={`${id}Label`}>{title}</Title>
                {children}
              </Content>
            </Dialog>
          </Wrapper>
        )}
      </AnimatePresence>,
      document.getElementById('modal-root')!
    )
  }

  return null
}
