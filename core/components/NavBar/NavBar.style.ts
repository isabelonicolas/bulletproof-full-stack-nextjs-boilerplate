import Link from 'next/link'
import styled from '@emotion/styled'
import { rem } from 'polished'

export const Wrapper = styled.nav`
  background-color: #ffffff;
  border-bottom: ${rem(1)} solid #f1f1f1;
`

export const Container = styled.div`
  ${({ theme }) => theme.mixins.container}

  display: flex;
  align-items: center;
  justify-content: space-between;
  height: ${rem(75)};
`

export const Brand = styled(Link)`
  display: block;
`

export const Nav = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`

export const NavItem = styled.li`
  display: inline-block;

  &:not(:last-child) {
    margin-right: ${rem(30)};
  }
`

export const NavLink = styled(Link)`
  color: #000000;
  display: block;
  text-decoration: none;
`
