import { Global, css, useTheme } from '@emotion/react'
import { normalize } from 'polished'

export const GlobalStyles = () => {
  const theme = useTheme()

  return (
    <Global
      styles={css`
        ${normalize()}

        :root {
          --app-height: 100vh;

          font-size: 16px;
        }

        html {
          box-sizing: border-box;
        }

        *,
        *:before,
        *:after {
          box-sizing: inherit;
        }

        html,
        body {
          width: 100%;
          margin: 0;
          padding: 0;
        }

        body {
          margin: 0;
          overflow: hidden;
          overflow-y: auto;

          color: #000000;
          font-family: ${theme.fonts.body};

          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
        }
      `}
    />
  )
}
