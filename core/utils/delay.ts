/**
 * A function for creating simulated delays
 *
 * @param {number} ms - The amount of delay in milliseconds
 * @returns {undefined}
 *
 * @example
 * import { delay } from 'core/lib/utils'
 *
 * async function asyncCall() {
 *   await delay(500)
 *   console.log('Hello world!')
 * }
 *
 */
export const delay = (ms: number) => new Promise((res) => setTimeout(res, ms))
