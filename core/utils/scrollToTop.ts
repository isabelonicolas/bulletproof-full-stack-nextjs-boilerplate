/**
 * Scrolls the page smoothly back to top and and sets the focus at the top of the page
 *
 * @returns {undefined}
 */
export const scrollToTop = () => {
  if (typeof window === 'undefined') return

  const element = document.querySelector('#top') as HTMLSpanElement

  if (!element) return

  element.focus({ preventScroll: true })

  element.scrollIntoView({
    behavior: 'smooth',
    block: 'end',
    inline: 'nearest',
  })
}
