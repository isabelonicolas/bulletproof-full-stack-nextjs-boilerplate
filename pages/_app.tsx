import type { AppProps } from 'next/app'
import React from 'react'
import { Provider as AppProvider } from 'react-redux'
import { CacheProvider, ThemeProvider } from '@emotion/react'
import createCache from '@emotion/cache'

import { NextPageWithLayout } from '../types/page'

import store from '@/redux/store'
import { theme } from '@/styles/theme'
import { gtmTrackingId } from '@/config/site'

import { GTMSetup } from '@/components/GTMSetup/GTMSetup'
import { GlobalStyles } from '@/components/GlobalStyles/GlobalStyles'

interface AppPropsWithLayout extends AppProps {
  Component: NextPageWithLayout
}

const cache = createCache({ key: 'next' })

const App = ({ Component, pageProps }: AppPropsWithLayout) => {
  const getLayout = Component.getLayout || ((page) => page)

  return (
    <>
      <GTMSetup trackingId={gtmTrackingId} />
      <AppProvider store={store}>
        <CacheProvider value={cache}>
          <ThemeProvider theme={theme}>
            <GlobalStyles />
            {getLayout(<Component {...pageProps} />)}
          </ThemeProvider>
        </CacheProvider>
      </AppProvider>
    </>
  )
}

export default App
