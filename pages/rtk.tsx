import { NextPage } from 'next'
import React from 'react'

import { useAppDispatch, useAppSelector } from '@/hooks/useApp'
import {
  decrement,
  increment,
  incrementByAmount,
} from '@/redux/features/counter/counterSlice'
import { getPosts } from '@/redux/features/posts/postsThunks'

const RTKPage: NextPage = () => {
  const dispatch = useAppDispatch()

  const counter = useAppSelector((state) => state.counter)
  const posts = useAppSelector((state) => state.posts)

  return (
    <div>
      <div>Count: {counter.value}</div>
      <button onClick={() => dispatch(incrementByAmount(10))}>
        Increment by 10
      </button>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <button onClick={() => dispatch(getPosts())}>Load Posts</button>

      <div>
        {posts.loading ? (
          <>Loading</>
        ) : (
          <>
            {posts.entities.length > 0 ? (
              <ul>
                {posts.entities.map((entity) => (
                  <li key={entity.id}>{entity.title}</li>
                ))}
              </ul>
            ) : (
              <p>No data found.</p>
            )}
          </>
        )}
      </div>
    </div>
  )
}

export default RTKPage
