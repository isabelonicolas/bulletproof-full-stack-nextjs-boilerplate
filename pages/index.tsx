import {
  GetStaticProps,
  GetStaticPropsContext,
  InferGetStaticPropsType,
} from 'next'

import { NextPageWithLayout } from '../types/page'
import { StandardLayout } from '@/components/Layout/StandardLayout'
import { SEOProps } from '@/components/SEO/SEO'

interface HomePageProps {
  seo: SEOProps
  content: string
}

const HomePage: NextPageWithLayout<
  InferGetStaticPropsType<typeof getStaticProps>
> = (props) => {
  const { content } = props

  return <div>{content}</div>
}

HomePage.getLayout = (page) => {
  const { seo } = page.props

  return <StandardLayout seo={seo}>{page}</StandardLayout>
}

export const getStaticProps: GetStaticProps<HomePageProps> = async (
  context: GetStaticPropsContext
) => {
  return {
    props: {
      seo: {
        title: `HomePage`,
        description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`,
      },
      content: `Content (HomePage)`,
    },
  }
}

export default HomePage
