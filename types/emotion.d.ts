import '@emotion/react'

declare module '@emotion/react' {
  export interface Theme {
    layout: {
      app: {
        height: string
      }

      container: {
        width: string
        gutter: string
      }

      column: {
        gutter: string
      }

      sidebar: {
        width: string
      }
    }

    breakpoint: {
      min: string
      xs: string
      sm: string
      md: string
      lg: string
      xl: string
      xxl: string
      xxxl: string
      fhd: string
    }

    media: {
      min: string
      xs: string
      sm: string
      md: string
      lg: string
      xl: string
      xxl: string
      xxxl: string
      fhd: string
    }

    mixins: {
      container: SerializedStyles
    }

    fonts: {
      body: string
    }
  }
}
